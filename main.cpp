#include <iostream>
#include "lib/httplib.h"
#include <vector>
#include <algorithm>
#include <fstream>

using namespace httplib;
using namespace std;

const string itemListFilename = "list.txt";

namespace{
    void writeItemListInFile(vector<string>& list){
        ofstream out;
        out.open(itemListFilename);
        for (auto item : list){
            out << item << ",";
        }
        out.close();
    }

    vector<string> loadItemList(){
        vector<string> items;
        try {
            ifstream in;
            in.open(itemListFilename);
            string fileContent;
            in >> fileContent;
            in.close();
            istringstream iss(fileContent);
            string token;
            while (getline(iss, token, ',')) {
                items.push_back(token);
            }
        } catch (exception& e){
            cout << "File not exist" << endl;
        }
        return items;
    }
}

int main() {

    Server server;
    vector<string> itemList;

    auto ret = server.set_mount_point("/ui", "..//web");
    if (!ret){
        cout << "directory do not exist" << endl;
    }


    server.Get("/author", [](const Request& req, Response& res){
        res.set_content("Bastien Chatelain", "text/plain");
    });

    server.Get("/add_item", [&itemList](const Request& req, Response& res){
        if(req.has_param("item")){
            try {
                auto val = req.get_param_value("item");
                itemList.push_back(val);
                writeItemListInFile(itemList);
            } catch (exception& e) {
                cout << "Invalid param item" << endl;
                res.set_content("Invalid param item", "text/plain");
            }
        }
    });

    server.Get("/list_items", [&itemList](const Request& req, Response& res){
        std::sort(itemList.rbegin(), itemList.rend());
        string string_list;
        for (auto item:itemList){
            string_list += item +" ";
        }
        res.set_content(string_list, "text/plain");
    });

    server.Get("/count_unique", [&itemList](const Request& req, Response& res){
            set<string> tempList;
            for (auto item : itemList){tempList.insert(item);}
            res.set_content(to_string((int) tempList.size()), "text/plain");
    });


    itemList = loadItemList();

    server.listen("localhost", 1234);

    cout << "HTTP REST API started at http://locahost:1234" << endl;

}
